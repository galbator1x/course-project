from copy import deepcopy
from io import BytesIO, StringIO
from lxml import etree as e
from django.conf import settings
from django.shortcuts import redirect
from django.contrib.admin.views.decorators import \
    staff_member_required as _staff_member_required
from django.db.models import Q, Sum
from django.template.response import TemplateResponse
from payments import PaymentStatus

from ..order.models import Order, Payment
from ..order import OrderStatus
from ..product.models import Product, ProductAttribute, ProductVariant


def staff_member_required(f):
    return _staff_member_required(f, login_url='account_login')


@staff_member_required
def index(request):
    orders_to_ship = Order.objects.filter(status=OrderStatus.FULLY_PAID)
    orders_to_ship = (orders_to_ship
                      .select_related('user')
                      .prefetch_related('groups', 'groups__items', 'payments'))
    payments = Payment.objects.filter(
        status=PaymentStatus.PREAUTH).order_by('-created')
    payments = payments.select_related('order', 'order__user')
    low_stock = get_low_stock_products()
    ctx = {'preauthorized_payments': payments,
           'orders_to_ship': orders_to_ship,
           'low_stock': low_stock}
    return TemplateResponse(request, 'dashboard/index.html', ctx)


@staff_member_required
def styleguide(request):
    return TemplateResponse(request, 'dashboard/styleguide/index.html', {})


def get_low_stock_products():
    threshold = getattr(settings, 'LOW_STOCK_THRESHOLD', 10)
    products = Product.objects.annotate(
        total_stock=Sum('variants__stock__quantity'))
    return products.filter(Q(total_stock__lte=threshold)).distinct()


def unload(request):
    output_path = '/home/makaveli/Downloads/course_project/db.xml'
    attrs = {attr.id: attr.name for attr in ProductAttribute.objects.all()}
    root = e.Element('документ')
    products = e.SubElement(root, 'товары')
    for p in Product.objects.all():
        product = e.Element('товар')
        e.SubElement(product, 'название').text = p.name
        e.SubElement(product, 'категория').text = p.categories.first().name
        e.SubElement(product, 'описание').text = p.description
        for attr_id, attr_val in p.attributes.items():
            attr = e.SubElement(product, 'характеристика')
            attr.set('название', attrs.get(int(attr_id)))
            attr.text = attr_val
        for variant in p.variants.all():
            _product = deepcopy(product)
            e.SubElement(_product, 'вариант').text = variant.name
            e.SubElement(_product, 'артикул').text = variant.sku
            e.SubElement(_product, 'цена').text = str(variant.get_price()[0])
            e.SubElement(_product, 'количество').text = str(variant.get_stock_quantity())
            products.append(_product)

    statuses = {'new': 'новый', 'cancelled': 'отменён', 'shipped': 'отправлен',
                'payment-pending': 'ожидает оплаты', 'fully-paid': 'оплачен'}
    orders = e.SubElement(root, 'заказы')
    for o in Order.objects.all():
        order = e.Element('заказ')
        e.SubElement(order, 'электроннаяПочта').text = o.get_user_current_email()
        e.SubElement(order, 'создан').text = str(o.created)
        e.SubElement(order, 'номер').text = str(o.id)
        e.SubElement(order, 'статус').text = statuses[o.status]
        e.SubElement(order, 'сумма').text = str(o.get_total()[0])
        e.SubElement(order, 'ценаДоставки').text = str(o.get_total_shipping()[0])
        shipping = e.SubElement(order, 'нужнаДоставка')
        shipping.text = 'да' if o.is_shipping_required() else 'нет'
        items = e.SubElement(order, 'товары')
        for item in o.get_items():
            p = item.product
            _item = e.SubElement(items, 'товар')
            e.SubElement(_item, 'название').text = p.name
            e.SubElement(_item, 'количество').text = str(item.quantity)
            e.SubElement(_item, 'сумма').text = str(item.get_total()[0])
            e.SubElement(_item, 'ценаЗаШтуку').text = str(item.get_price_per_item()[0])
            variant = ProductVariant.objects.get(id=item.stock_id)
            e.SubElement(_item, 'артикул').text = str(variant.sku)
            e.SubElement(_item, 'вариант').text = variant.name
        if o.is_shipping_required():
            addr = o.shipping_address
            address = e.SubElement(order, 'адресДоставки')
            e.SubElement(address, 'имя').text = addr.first_name
            e.SubElement(address, 'фамилия').text = addr.last_name
            e.SubElement(address, 'компания').text = addr.company_name
            e.SubElement(address, 'адрес1').text = addr.street_address_1
            e.SubElement(address, 'адрес2').text = addr.street_address_2
            e.SubElement(address, 'город').text = addr.city
            e.SubElement(address, 'индекс').text = addr.postal_code
            e.SubElement(address, 'кодСтраны').text = addr.country.code
            e.SubElement(address, 'область').text = addr.country_area
            e.SubElement(address, 'телефон').text = addr.phone
        addr = o.billing_address
        address = e.SubElement(order, 'платёжныйАдрес')
        e.SubElement(address, 'имя').text = addr.first_name
        e.SubElement(address, 'фамилия').text = addr.last_name
        e.SubElement(address, 'компания').text = addr.company_name
        e.SubElement(address, 'адрес1').text = addr.street_address_1
        e.SubElement(address, 'адрес2').text = addr.street_address_2
        e.SubElement(address, 'город').text = addr.city
        e.SubElement(address, 'индекс').text = addr.postal_code
        e.SubElement(address, 'кодСтраны').text = addr.country.code
        e.SubElement(address, 'область').text = addr.country_area
        e.SubElement(address, 'телефон').text = addr.phone
        orders.append(order)
    with open(output_path, 'w') as w:
        w.write(e.tounicode(root, pretty_print=True))
    return redirect(request.META['HTTP_REFERER'])
